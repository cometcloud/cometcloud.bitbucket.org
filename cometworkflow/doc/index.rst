Welcome to CometCloud Workflow
================================


CometCloud workflow extends `CometCloud <http://cometcloud.org>`_ to enable scientists with mechanisms that ease the autonomic execution of complex workflows in software-defined multi-cloud environments. This framework allows users to define objectives and policies to drive the federation of resources as well as the workflow execution. The resulting solution is a platform that takes a workflow description from the user and autonomously orchestrates the execution of such a workflow by elastically composing appropriate cloud services and capabilities to ensure that the user’s objectives are met.

This framework implements a programmable dynamic federation model that uses software-defined environment concepts to drive the federation process and seamlessly adapt resource compositions at runtime. In this way, users and providers can define high-level policies and rules that regulate the use of the resources. For example, users may prefer to use a certain type of resources over others (e.g., HPC versus clouds or “free” HPC systems versus the allocation-based ones); they may want to reserve certain resources for specific projects; they may be only allowed to use certain resources at certain times of the day; they may want to use cloud resources only if they are within a desired price range; they may want to specify how to react to unexpected changes in the resource availability or performance; or they may want to only use resources within the US or Europe due to the laws regulating data movement across borders.




  .. toctree::	
     :maxdepth: 2
     :hidden:
        
     whatisit
     quickStart
     documentation
     downloads
     support
     todos

	
.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

 
     
    
    



.. _configfilereference:

Configuration File Reference
============================

.. _workflow_config:


File ``manager.properties``
---------------------------

publicIpManager
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Public IP or FQDN of the workflow Manager. This will be used for  other services or clients to contact with this service. E.g., dell01.domain.edu or 129.12.1.23 


portManager
~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where this service will be listening.


logFile
~~~~~~~

**Type:** String

**Required:** Yes

Path to file where the logs of the service will be stored.


workflowmasterURI
~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Address and port of the Task Manager service is running. Format is <ip or fqdn>:<port>. This service is based on the CometCloud master. E.g., machine1.domain.edu:7777. 

CentralManagerAddress
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Public Ip of the autonomic scheduler/central resource manager is running. Currently, this address has to be the same as publicIpManager because both services are started together. However, it is prepared to have them as independent services when needed.


CentralManagerPort
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the Autonomic Scheduler/Central resource manager service is listening.


MonitorInterval
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Interval in seconds to monitor status of agents/sites. This is used by the autonomic scheduler to monitor resource status and analyze if any actions is required to guarantee QoS

DefaultReferenceBenchmark
~~~~~~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

Default benchmark score that can be used to calculate the speedup of all available resources (speedup = <resourceBenchmarkScore>/<DefaultBenchmarkScore>). ``DefaultBenchmarkScore`` is only used if the user does not indicate a benchmark score when submitting a workflow and when the metrics service is disabled or it does not have information about the application to be executed. If DefaultBenchmarkScore is not defined a value of 5757 would be assigned by default.

******************

.. _master_config:


File ``master.properties``
--------------------------

portManager
~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the Task Manager will be listening for instructions from the Workflow Manager and Autonomic Scheduler.


logFile
~~~~~~~

**Type:** String

**Required:** No

Path to file where the logs of the Task Manager will be stored.


logFileRH
~~~~~~~~~

**Type:** String

**Required:** No

Path to file where the logs of the Request Handler will be stored.


File ``comet.properties``
-------------------------

MasterClass
~~~~~~~~~~~

**Type:** String

**Required:** Yes

This property defines the Master application in CometCloud. In our case, this application is the Task Manager (or WorkflowMaster). This property should be *tassl.application.cometcloud.WorkflowMaster*, unless you know what you are doing.

WorkerClass
~~~~~~~~~~~

**Type:** String

**Required:** Yes

This property defines the Worker application in CometCloud. This is used only if we distribute the CometCloud space across more than one machine. This property should be *tassl.application.cometcloud.AppWorker*, unless you know what you are doing.

TaskClass
~~~~~~~~~

**Type:** String

**Required:** Yes

This property defines the class that represents Task Tuples in CometCloud. This is used by CometCloud to identify tasks and allow workers to retrieve tasks. This property should be *tassl.application.cometcloud.WorkflowTaskTuple*, unless you know what you are doing.


RoutingKeys
~~~~~~~~~~~

**Type:** String

**Required:** Yes

Routing keys defined in squid.properties and chord.properties. Default name to identify the Id of a task, hence the value should be *TaskId*.

TaskMonitoringPeriod
~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

Period to check missing tasks in the CometCloud space (milliseconds). This is disabled if its value is 0.

IsolatedProxy
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

This property indicates the machine that has the CometCloud proxy and hence enables access to query and retrieve tasks from the CometCloud Space.


IsolatedProxyPort
~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

This port is used by Agents to contact CometCloud proxy and request tasks. Its default value is **12342**.

REQUEST_HANDLER_PORT
~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

This port is used by the CometCloud proxy to contact a request handler and communicate Agents’ task requests. Its default value is **12343**.

TCP_CONTROL_PORT
~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

This port is used by Agents to communicate completed tasks and their result to the Task Manager. Its default value is **12340**.

COMET_CONTROL_PORT
~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** No

This port is used by CometCloud to control its services. Its default value is **12330**.


***************

.. _agent_config:

File ``agent.properties``
-------------------------

publicIpAgent
~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Public IP or FQDN of the Agent. This will be used for other services to contact with this service. E.g., machine3.domain.com or 129.12.1.23 

portAgent
~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the agent service will be listening.

publicIPSite
~~~~~~~~~~~~~

**Type:** String

**Required:** No

In this case the ports of the agent machine to the outside world are closed and we cannot use the public IP or fqdn. In this case we need to set ``publicIpAgent`` equal to ``localhost`` and define this parameter with the public IP or fqdn. This ensures that generated files are accessible to others.

logFile
~~~~~~~

**Type:** String

**Required:** Yes

Path to file where the logs of the service will be stored.

MaxNoTaskRetries
~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Maximum number of times that a worker asks for tasks and CometCloud replies that there is no tasks for that query. After this worker will stop asking and enter in sleeping mode.

StartSshTunnelWorkers
~~~~~~~~~~~~~~~~~~~~~

**Type:** Boolean

**Required:** No

**Accepted Values:** {true, false}

In case the workers cannot contact directly with the machine where the Agent is running (e.g., sometimes cluster nodes cannot contact the front-end), we can create reverse SSH tunnels from the Agent towards the workers. If this is `true` we have to define the property `IpAgentForWorkers=localhost`. If this property is not set then its value is `false`.

IpAgentForWorkers
~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** No

Private IP the Agent. This will be used for other services to contact with this service. E.g., 192.168.1.1. If `StartSshTunnelWorkers=true`, then this has to be **localhost**.


StartSshTunnelAgent
~~~~~~~~~~~~~~~~~~~

**Type:** Boolean

**Required:** No

**Accepted Values:** {true, false}

In case the agent cannot contact directly with the machine where the workers are running (e.g., sometimes the ports of cluster nodes are filtered like in OSG), we can create a SSH tunnel from the Agent towards the worker machine. If this is `true` we have to define the property `SshTunnelAgentPortRange`. If this property is not set then its value is `false`.

SshTunnelAgentPortRange
~~~~~~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** No

Range of ports that will be used when creating a SSH tunnel between the agent and the worker in the remote resources. Note that this range should have as many ports as simultaneous workers we pretend to run. This is only used when `StartSshTunnelAgent` property is `true`. Format is <initialPort>:<lastPort>

StartFileServer
~~~~~~~~~~~~~~~

**Type:** Boolean

**Required:** No

**Accepted Values:** {true, false}

Decide if File server should be started. This server is used to transfer files automatically to the resources when the workers need them (uses rsync over SSH). If this property is not set then its value is `false`.


FileServerPort
~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the file server of the Agent will be listening.

FileServerMaxThreads
~~~~~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Maximum number of concurrent petitions the server can handle.

StageFileDir
~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Directory that will be used as staging area in this site. If several workers ask for the same file, this file is only retrieved from the remote location once, and then it is stored here.

StartManager
~~~~~~~~~~~~

**Type:** Boolean

**Required:** Yes

**Accepted Values:** {true, false}

Decide if the Local resource manager is started. This server is used to dynamically provision resources.

MgmtPort
~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the Local resource manager service is listening. This service is contacted by the Autonomic scheduler to provision and terminate resources.

CentralManagerServer
~~~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Address and port of the Central Manager Server/Autonomic scheduler. Format is <ip or fqdn>:<port>

MonitorInterval
~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Interval in seconds to monitor status of resources.

Resources
~~~~~~~~~

**Type:** String

**Required:** Yes

Type of resource and file that contains the specific configuration. Format is <type>:<file>. Type can be cluster or cloud.


UseMeticsService
~~~~~~~~~~~~~~~~

**Type:** Boolean

**Required:** No

**Accepted Values:** {true, false}

Decide if the metrics service is used. By default it is false.


AgentMetricsAddress
~~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes if UseMeticsService=true

Address where the Metrics service is hosted.

AgentMetricsPort
~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes if UseMeticsService=true

Port where the Metrics service is listening.

DBName
~~~~~~

**Type:** String

**Required:** Yes if UseMeticsService=true

Name of the database that stores the information of this agent.


File ``ClusterDell``
--------------------


Name
~~~~

**Type:** String

**Required:** Yes

Name of the site

Zone
~~~~
**Type:** String

**Required:** Yes

Zone that this site is part of. This is used to control data movement.

QueueType
~~~~~~~~~
**Type:** String

**Required:** No

Type of queue system (Not functional at this moment). Currently we assume we assume we have ssh access to all nodes of the cluster.

QueueName
~~~~~~~~~
**Type:** String

**Required:** No

Name of the queue to use (Not functional).

Overhead
~~~~~~~~

**Type:** Integer

**Required:** Yes

Estimation of the time needed to provision resources (seconds). This is used for initial estimation, then actual values should be recorded and updated.


WorkerLimit
~~~~~~~~~~~

**Type:** String

**Required:** Yes

Number of workers per machine. It is recommended to use only one per machine, more than one is experimentally supported and might have bugs. In the example we have two machines and we can provision one worker in each one. Format is <ip>:<number>;<ip>:<number>.


Cost
~~~~

**Type:** String

**Required:** Yes

Cost for each type of resource. The cost is per hour. Format is <ip>:<number>;<ip>:<number>

BenchmarkScore
~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Benchmark score of each machine. This is used to estimate the performance of each machine. Any benchmark can be used for this, e.g., the Whetstone Score (UnixBench) for a t2.medium instance in AWS is 8200. This value is only used when there is no real data regarding to a specific application. Once the agent has real data this score is ignored. Note that in order for the agent to obtain real data from application executions the Metrics service must be enabled.

workerPortRange
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Range of ports that will be assigned to workers started in the remote resources. Note that this range is independent for each resource and therefore two different machines can reuse ports. Format is <initialPort>:<lastPort>


SupportedApps
~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

We specify a list of applications this resource can handle, and the CometCloud worker class that contains the logic. The name of this applications are the ones that need to be used in the ``Application`` field of the workflow definition (see :ref:`Defining Workflows <workflowdefinition>`). The format is <appname>,<appname1>. Note that for each application we need to include the classpath of its worker. For example, if we have `SupportedApps=testapp1,testapp2`, we have to include two additional lines to this file (1) `testapp1=tassl.application.cometcloud.sample.AppWorker` and (2) testapp2=tassl.application.cometcloud.anothersample.AppWorker.

SoftwareDirWorker
~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Software directory in **worker machine**. Directory where the software is expected to be in the resource for each application. For example, the software of testapp1 is expected to be in the path `<SoftwareDirWorker>/testapp1/`. Inside that directory we must have any binary required by our application plus the dist and lib directories with the CometCloud jar files as well as the specific worker application jar.

UserWorker
~~~~~~~~~~

**Type:** String

**Required:** Yes

User in **worker machine**. This is the user that we will use to ssh into the worker machine. It must be configure to enable ssh with no password.

WorkingDir
~~~~~~~~~~

**Type:** String

**Required:** Yes

Directory inside the **worker machine** that will be used to transfer input files and write any data generated during the execution of the application.



File ``cloudOSSierra``
----------------------

Name
~~~~

**Type:** String

**Required:** Yes

Name of the site

Zone
~~~~
**Type:** String

**Required:** Yes

Zone that this site is part of. This is used to control data movement.


Key
~~~~
**Type:** String

**Required:** No

This key is used to interact with the virtual machines (VMs) of the cloud. Typically, when using cloud technology you need to specify the a keypair that will allow ssh to the VM. The **key file** must have the same **name** as the keypair in the cloud (you can see your cloud keypairs in the portal-security or using the command line, e.g., euca-describe-keypairs, nova keypait-list). The extension (`.pem` in this case) is not important. Also make sure that the **permission** of the key file are correct (e.g., `chmod 0600 jdiaznova.pem`).

CloudScript
~~~~~~~~~~~

**Type:** String

**Required:** Yes

Path to the plugin that enables provisioning and deprovisioning cloud resources. Currently we provide with one called **cloud.py** that supports some of the major cloud platforms.

ProviderType
~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Since the `CloudScript` supports multiple platforms, we need to specify which one we will be using. The ProviderType table in :ref:`Configuring a Cloud section <configureCloud>` summarizes the current options defines the type of cloud provider we use. We use `openstack_ec2` for system such as FutureSystem OpenStack; `openstack_nova` for Chameleon OpenStack Alamo; `aws_ec2` for AWS; and `nimbus_ec2` to interact with Nimbus EC2 interface.


Region
~~~~~~

**Type:** String

**Required:** Yes

Cloud providers usually require specifying a region in which you will be operating. Some examples of current platforms are described in the following table. Some examples are `nova` for FutureSystems OpenStack, `regionOne` for Chameleon OpenStack Alamo, or us-west-2 for WS (US West -Oregon).

  
ProviderConfigFile
~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

This file contains the configuration provided for your cloud provider. Some examples are provided in :ref:`Cloud configuration Files section <cloud_configFiles>`.

VMLimits
~~~~~~~~

**Type:** String

**Required:** Yes

Determine the type of virtual machine (VM) supported and the maximum number of each type that is available. Format is <type>:<number>;<type>:<number>. For example if we define `VMLimits=m1.small:2;m1.medium:5;m1.large:3`, this site will be able to launch a maximum of 2 VMs of m1.small type, 5 VMs of m1.medium type, and 3 VMs of m1.large type.

Overhead
~~~~~~~~

**Type:** Integer

**Required:** Yes

Estimation of the time needed to provision resources (seconds). This is used for initial estimation, then actual values should be recorded and updated.


WorkerLimit
~~~~~~~~~~~

**Type:** String

**Required:** Yes

Number of workers per type of VM. It is recommended to use only one per machine, more than one is experimentally supported and might have bugs. In the example we have three types of VMs and we can provision one worker in each one. Format is <type>:<number>;<type>:<number>.


Cost
~~~~

**Type:** String

**Required:** Yes

Cost of a VM per unit of time (i.e. hour). We can specify a different cost for each machine. Format is <type>:<number>;<type>:<number>

BenchmarkScore
~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Benchmark score of each machine. This is used to estimate the performance of each machine. Any benchmark can be used for this, e.g., the Whetstone Score (UnixBench) for a t2.medium instance in AWS is 8200. This value is only used when there is no real data regarding to a specific application. Once the agent has real data this score is ignored. Note that in order for the agent to obtain real data from application executions the Metrics service must be enabled.

CostDataIn
~~~~~~~~~~

**Type:** Double

**Required:** Yes

Cost of transferring data inside the site. Cost per GB of data.

CostDataOut
~~~~~~~~~~~

**Type:** Double

**Required:** Yes

Cost of transferring data outside the site. Cost per GB of data.

workerPortRange
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Range of ports that will be assigned to workers started in the remote resources. Note that this range is independent for each resource and therefore two different machines can reuse ports. Format is <initialPort>:<lastPort>


SupportedApps
~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

We specify a list of applications this resource can handle, and the CometCloud worker class that contains the logic. The name of this applications are the ones that need to be used in the ``Application`` field of the workflow definition (see :ref:`Defining Workflows <workflowdefinition>`). The format is <appname>,<appname1>. Note that for each application we need to include the classpath of its worker. For example, if we have `SupportedApps=testapp1,testapp2`, we have to include two additional lines to this file (1) `testapp1=tassl.application.cometcloud.sample.AppWorker` and (2) testapp2=tassl.application.cometcloud.anothersample.AppWorker.


defaultImageId
~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

This is the id of the VM image that will be deployed by default as a worker machine. This VM image has to be prepared beforehand with the proper software and libraries. More information is provided in :ref:`Cloud VM image configuration section <cloud_configVM>`. 

testapp1ImageId
~~~~~~~~~~~~~~~

**Type:** String

**Required:** No

We can also specify different VMs images for different applications. The format is `<application>ImageId`. In our example, we have a specific image for the `testapp1` application (``testapp1ImageId``), while the `testapp2` application uses the default one.


SoftwareDirWorker
~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Software directory in **worker machine**. Directory where the software is expected to be in the resource for each application. For example, the software of testapp1 is expected to be in the path `<SoftwareDirWorker>/testapp1/`. Inside that directory we must have any binary required by our application plus the dist and lib directories with the CometCloud jar files as well as the specific worker application jar.

UserWorker
~~~~~~~~~~

**Type:** String

**Required:** Yes

User in **worker machine**. This is the user that we will use to ssh into the worker machine. It must be configure to enable ssh with no password.

WorkingDir
~~~~~~~~~~

**Type:** String

**Required:** Yes

Directory inside the **worker machine** that will be used to transfer input files and write any data generated during the execution of the application.

***************

.. _metrics_config:

File ``metrics.properties``
----------------------------

AgentMetricsAddress
~~~~~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Public IP or FQDN of the Metrics service. This will be used for other services to contact with this service. E.g., metrics.domain.com or 129.12.1.29

AgentMetricsPort
~~~~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the metrics service will be listening.

DBAddress
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Public IP or FQDN of the database.

DBPort
~~~~~~~~~~~~~

**Type:** Integer

**Required:** Yes

Port where the database service will be listening.

DBType
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Type of database used for persistence. Currently only MongoDB is supported identified by ``mongodb``.

logFile
~~~~~~~~~~~~~~~

**Type:** String

**Required:** Yes

Name of the file that stores logs for the Metrics service



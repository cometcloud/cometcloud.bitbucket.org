.. _sec_support:

Support
-------

If you run into problems when using CometCloud Workflow, contact us  cometcloud [at] cac.rutgers.edu


Known Issues
------------

* CometCloud is compiled with Java 1.6 as it is the most extended version in current linux versions. We have noticed issues when compiling CometWorkflow or a CometWorkflow application using Java 1.8


.. _chap_config_workflow:

Configuring Workflow Manager and Autonomic Scheduler
****************************************************

The workflow manager is the entry point to the system and it will orchestrate the execution of workflows. Currently, the autonomic scheduler is 
launched by the workflow manager, although it is totally independent. The autonomic scheduler decides where to schedule tasks, which resources to provision,
and keeps the information of the resources.

Next we present a configuration file example, where a workflow manager is going to be deployed in the machine defined in ``publicIpManager``, port ``portManager``. The logs are stored in the file defined by ``logFile``, the Task Manager (aka. Workflow Master) is listening in ``workflowmasterURI`` (i.e. machine machine2.domain.com, port 7777). The Autonomic scheduler is going to be deployed in the machine defined in ``CentralManagerAddress`` (currently, this property has to be the same than ``publicIpManager``), port ``CentralManagerPort``. Additionally, we define a monitoring interval of 60 seconds, which is used by the autonomic scheduler to monitor resource status and analyze if any actions is required to guarantee QoS. Additionally we can include a default benchmark score that can be used to calculate the speedup of all available resources (speedup = <resourceBenchmarkScore>/<DefaultBenchmarkScore>). More information about this configuration file can be found in :ref:`WORKFLOW section <workflow_config>`. For simplicity we keep scripts and examples of the configuration files in the directory ``simple_run/workflow/``

Requirements
------------

This service requires Java runtime 1.7+ and rsync.

.. _manager.properties:

Edit ``manager.properties`` file
------------------------------------------

   .. code-block:: bash

	publicIpManager=machine1.domain.com
	portManager=8888

	logFile=/path/to/logfile/WorkflowManager.log

	#task generator(workflowMaster)
	workflowmasterURI=machine2.domain.com:7777

	#resource manager and autonomic scheduler
	StartCentralManager=true
	CentralManagerAddress=machine1.domain.com
	CentralManagerPort=7778

	MonitorInterval=60

    DefaultBenchmarkScore=5757

.. note::

   ``DefaultBenchmarkScore`` is only used if the user does not indicate a benchmark score when submitting a workflow and when the metrics service is disabled or it does not have information about the application to be executed.

Starting the Service
--------------------

Once the configuration is ready, you can start the workflow manager and autonomic scheduler by executing the script ``startWorkflowManager.sh``. This script contains the following code that defines the CLASSPATH and executes the appropriated java class.

   .. code-block:: bash

       export CLASSPATH=../../dist/*:../../lib/*
       java -cp $CLASSPATH tassl.application.workflow.WorkflowManager -propertyFile manager.properties


.. note::

   These services can start regardless the status of other services of our federation. It is not until a workflow is started that this service contacts the Task Manager (aka. Workflow Master).

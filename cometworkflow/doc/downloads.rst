.. _sec_download:

Download Software
*****************

You can download the latest compiled version of the code and examples from `Download Page <http://nsfcac.rutgers.edu/request-software>`_ and selecting CometCloud Software option

.. You can download the latest compiled version of the code from :download:`here <_downloads/cometWorkflow.tgz>`

.. Application examples, which are included in the compiled code, can be downloaded from :download:`here <_downloads/cometWorkflowExample.tgz>`

You can obtain the source code from `BitBucket <https://bitbucket.org/cometcloud/cometworkflow>`_

.. _chap_documentation:

Documentation
=================================

User Documentation
------------------

In this section you will find information on how to use the command line interface and how to define workflows.

.. toctree::
    :maxdepth: 1
    
    workflowdefinition
    workflowclient
    
   
Administrator Documentation
---------------------------

In this section you will find information regarding software deployment.

.. toctree::
    :maxdepth: 2
    
    configure_workflow   
    configure_master
    configure_agent
    configure_metrics
    configInfo
    


Developer Documentation
-----------------------

In this section you will find information about the various application programming interfaces (API), which will help you to develop new applications, create new policies, and create new platform plugin. 

.. toctree::
    :maxdepth: 2
    
    develop_app
    develop_policy


JavaDoc Documentation
---------------------

* `CometWorkflow JavaDoc <_static/javadoc/index.html>`_


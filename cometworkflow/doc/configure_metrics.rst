.. _chap_metricsserviceConf:

Configuring Metrics Service
***************************

The Metrics Service allows Agents to store historical information about various metrics. Currently, we can store raw data regarding resource performance for different applications, failure rates, provisioning overheads, network performance between Agents and between an Agent and its workers. A federated deployment could have multiple Metric services to serve different Agents. A Metrics service can store its information in a database and it is able to generate metrics using the raw data sent by Agents.

Next we describe how to configure a Metrics Service in a site called **metrics.domain.com** and detail the different configuration files involved. More information about this section configuration files can be found in :ref:`Metrics section <metrics_config>`. For simplicity we keep scripts and examples of the configuration files in the directory ``simple_run/metrics/``


Requirements
------------

This service requires Java runtime 1.7+.


Edit ``metrics.properties`` file
---------------------------------

This file contains the information required to configure the Metrics service. ``AgentMetricsAddress`` and ``AgentMetricsPort`` indicate the address and port where this service will be listening. ``DBAddress`` and ``DBPort`` indicate the address and port where the database is listening. ``DBType`` indicates the type of database (currently only MongoDB is supported). Finally, ``logFile`` defines the file where logs will be stored.

   .. code-block:: bash

      AgentMetricsAddress=metrics.domain.com
      AgentMetricsPort=8891
      DBPort=27017
      DBAddress=localhost
      DBType=mongodb
      logFile=AgentMetricsService.log


Configure MongoDB
-----------------

MongoDB is currently the only database supported. Ideally you will want to have a fault tolerance deployment of MongoDB as indicated in their `website <https://docs.mongodb.com/manual/installation/>`_. However, for testing purposes we are going to deploy MongoDB in the same machine where the Metrics service is running using the following `MongoDB guide <https://docs.mongodb.com/manual/tutorial/install-mongodb-on-linux/>`_. Make sure to start the service using ``--bind_ip`` option to avoid security problems


	.. code-block:: bash

	    mongod --bind_ip 127.0.0.1 --dbpath /home/cc/db/ --logpath /home/cc/db/mongo.log